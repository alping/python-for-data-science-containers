# Python for Data Science - Containers

Docker containers for use as the base for Python Data Science projects. Comes installed with:

- [Micromamba](https://mamba.readthedocs.io/)
- [Quarto](https://quarto.org/)

---

- **gitpod.Dockerfile** - For use with [gitpod.io](https://gitpod.io/)
