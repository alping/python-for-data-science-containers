FROM gitpod/workspace-base:latest

# Install Dependencies
RUN sudo install-packages gdebi-core

USER gitpod

# Install Micromamba
RUN curl micro.mamba.pm/install.sh | bash && \
    echo "alias conda='micromamba'" >> ~/.bashrc && \
    echo "alias mamba='micromamba'" >> ~/.bashrc

# Install Quarto
RUN curl -LO https://quarto.org/download/latest/quarto-linux-amd64.deb && \
    sudo gdebi -n quarto-linux-amd64.deb && \
    rm quarto-linux-amd64.deb
